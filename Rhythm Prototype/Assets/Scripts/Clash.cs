﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clash : MonoBehaviour {
    public GameObject explosion;
	// Use this for initialization
	void Start () {
		
	}
	
	void OnTriggerEnter(Collider other)
    {
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
	void Update () {
		
	}
}
