﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroll : MonoBehaviour
{
    public Transform[] Endpoint;
    public float speed;
    private int current;

    

    
	
	// Update is called once per frame
	void Update ()
    {
        if (transform.position != Endpoint[current].position)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, Endpoint[current].position, speed * Time.deltaTime);
            GetComponent<Rigidbody>().MovePosition(pos);
        }
        else current = (current + 1) % Endpoint.Length;
    }
}